const {User} = require('../db/User')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const JWT_SECRET = process.env.JWT_SECRET || 'asd'

exports.getToken = function(user) {
	return jwt.sign({
        _id: user._id
    }, JWT_SECRET)
}

exports.logIn = function(user) {
    return {
        token: exports.getToken(user),
        user
    }
}

exports.getUser = async function(token) {
	if(!token){
		throw {
            status: 403,
            message: 'User is not authenticated'
        }
	}
	const {_id} = jwt.verify(token, JWT_SECRET)
	const user = await User.findOne({
		_id
	})
    if(!user) {
        throw {
            status: 403,
            message: 'User is not authenticated'
        }
    }
	return user
}

exports.loginWithPassword = async function(email, password){
    let user = await User.findOne({
        email
    })
    if(!user){
        throw ({
            status: 400,
            message: 'User is not found'
        })
    }
    if(!user.verified) {
    	throw  ({
            status: 403,
            message: 'User is not verified'
        })
    }
    if(await exports.matchPassword(password, user.password_hash))
        return exports.logIn(user)
    throw  ({
        status: 400,
        message: 'Password mismatch'
    })
}

exports.matchPassword = function(password, password_hash) {
	return new Promise((resolve, reject)=>{
		bcrypt.compare(password, password_hash, (err, res)=>{
			if (err) {
				reject(err)
			} else {
				resolve(res)
			}
		})
	})
}

module.exports = exports