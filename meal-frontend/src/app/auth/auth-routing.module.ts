import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RecoverComponent } from './recover/recover.component';
import { PasswordComponent } from './password/password.component';
import { VerificationComponent } from './verification/verification.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent, data: { title: 'Login' } },
    { path: 'register', component: RegisterComponent, data: { title: 'Sign Up' } },
    { path: 'recover', component: RecoverComponent, data: { title: 'Recover' } },
    { path: 'password/:code', component: PasswordComponent, data: { title: 'Recover' } },
    { path: 'verify/:code', component: VerificationComponent, data: { title: 'Verification' } }
];
@NgModule({
  imports: [
  	RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
