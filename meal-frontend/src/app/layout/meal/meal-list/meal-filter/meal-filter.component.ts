import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'meal-filter',
  templateUrl: './meal-filter.component.html',
  styleUrls: ['./meal-filter.component.css']
})
export class MealFilterComponent implements OnInit {

  @Output() dataUpdate: EventEmitter<MealFilterEvent> = new EventEmitter();
  @Input() showUserInput: boolean;
  constructor() { }

  ngOnInit() {

  }

  startDate: Date;
  endDate: Date;
  startTime: Date;
  endTime: Date;
  name: string;

  filter() {
	  this.dataUpdate.emit({
      startDate: this.startDate,
      endDate: this.endDate,
      startTime: this.resetDate(this.startTime),
      endTime: this.resetDate(this.endTime),
      name: this.name
    })
  }

  reset() {
    this.startDate = null
    this.endDate = null
    this.startTime =null
    this.endTime = null
    this.name = ""
    this.filter()
  }


  resetDate(d) {
    if(d){
      return d.getHours()*60+d.getMinutes()
    }
    return ""
  }
}

interface MealFilterEvent {
	startDate: Date,
  	endDate: Date,
  	startTime: string,
  	endTime: string,
    name: string
} 
