import { Component, OnInit, OnDestroy } from '@angular/core';
import { CaloriesService } from '../calories.service'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription'

@Component({
  selector: 'app-calories-dialog',
  templateUrl: './calories-dialog.component.html',
  styleUrls: ['./calories-dialog.component.css']
})
export class CaloriesDialogComponent implements OnInit, OnDestroy {
  caloriesForm: FormGroup;
  constructor(
  	private caloriesService: CaloriesService,
  	private _fb: FormBuilder,
  	public dialogRef: MatDialogRef<CaloriesDialogComponent>
  ) { }
  obs: Subscription
  ngOnInit() {
  	this.caloriesForm = this._fb.group({
  		calories: [null, [
        Validators.required,
        Validators.min(1),
        Validators.max(1000000000),
      ]]
  	});
    this.obs = this.caloriesService.expectedCalories.subscribe((x)=>{
      console.log(x, 'caloriesService.expectedCalories')
      this.caloriesForm.controls['calories'].setValue(x)
    })

  }

  ngOnDestroy() {
    this.obs.unsubscribe()
  }

  save() {
  	if(this.caloriesForm.valid){
  		this.caloriesService.changeExpectedCalories(this.caloriesForm.value.calories).subscribe(()=>{
        this.dialogRef.close()
      })
  		
  	}
  }

  onNoClick(): void {
	  this.dialogRef.close()
  }
}
