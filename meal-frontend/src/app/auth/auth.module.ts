import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { RecoverComponent } from './recover/recover.component';
import { PasswordComponent } from './password/password.component';
import { RegisterComponent } from './register/register.component';
import { VerificationComponent } from './verification/verification.component';

import { CoreModule } from '../core/core.module'

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    CoreModule
  ],
  providers: [],
  declarations: [
      LoginComponent,
      RecoverComponent,
      PasswordComponent,
      RegisterComponent,
      VerificationComponent
  ]
})
export class AuthModule { }
