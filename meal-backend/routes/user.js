const Router = require('koa-router');
const router = new Router({
	prefix: '/users'
});

const auth = require('../middleware/auth')

const UserService = require('../services/UserService')
const AuthService = require('../services/AuthService')

router.post('/', async function(ctx, next) {
    console.log('asd')
    if(ctx.request.body.isModerator) {
        await auth({
            isModerator: true
        })(ctx, next)
    } else {
        await next()
    }
}, async function (ctx, next) {
	//ctx.body='user'
    const data = {
        email: ctx.request.body.email ? ctx.request.body.email.toString() : "",
        name: ctx.request.body.name ? ctx.request.body.name.toString() : "",
        password: ctx.request.body.password ? ctx.request.body.password.toString() : "",
    }

    if(ctx.user && ctx.user.isModerator()) {
        data.verified = !!ctx.request.body.verified
        data.role = ctx.request.body.role || 'user'
    }

	const user = await UserService.addUser(data)
    //const auth = AuthService.logIn(user)
    ctx.body = user;
})

router.post('/verify', async function (ctx, next) {
    //ctx.body='user'
    if(ctx.request.body.code){
        ctx.request.body.code = ctx.request.body.code.toString()
    }
    const user = await UserService.verify(ctx.request.body.code || "")
    const auth = AuthService.logIn(user)
    ctx.body = auth;
})

router.get('/', auth({
    isModerator: true
}), async function (ctx, next) {
    ctx.body = await UserService.getUsers(ctx.user, ctx.request.query)
})


router.put('/me', auth(), async function (ctx, next) {
    const user = await UserService.editUser(ctx.user, ctx.user._id, {
        expectedCalories: ctx.request.body.expectedCalories
    })
    ctx.body = user;
})

router.put('/:id', auth({
    isModerator: true
}), async function (ctx, next) {
    const user = await UserService.editUser(ctx.user, ctx.params.id, {
        name: ctx.request.body.name,
        verified: ctx.request.body.verified,
        role: ctx.request.body.role
    })
    ctx.body = user;
})

router.delete('/:id', auth({
    isModerator: true
}), async function (ctx, next) {
    await UserService.deleteUser(ctx.user, ctx.params.id)
    ctx.body = {};
})

module.exports = router.routes()