const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const shortid = require('shortid')

const passwordSchema = mongoose.Schema({
    code: {
        type: String,
        required : true,
        default: function() {
            return shortid.generate()
        }
    },
    status: {
        //0 open
        //1 used
        //2 canceled
        //3 timeout
        type: String,
        required : true
    },
    createdDate: {
        type: Date,
        required : true
    },
    usedDate: {
        type: Date
    },
    userId: {
        type: String,
        required : true
    }
});

const PasswordRecovery = mongoose.model('PasswordRecovery', passwordSchema);

module.exports = PasswordRecovery