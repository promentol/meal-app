import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'user-filter',
  templateUrl: './user-filter.component.html',
  styleUrls: ['./user-filter.component.css']
})
export class UserFilterComponent implements OnInit {
  @Output() dataUpdate: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  onSearchChange(q) {
	this.dataUpdate.emit({q})
  }

}
