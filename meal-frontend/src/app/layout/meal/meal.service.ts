import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meal } from './meal.model'
import { Subject } from 'rxjs/Subject'
import { environment } from '../../../environments/environment';

@Injectable()
export class MealService {

  mealUrl = `${environment.API_URL}/meals`
  public dataChange$: Subject<number> = new Subject();
  constructor(public http: HttpClient) { }

  addMeal(data) {
  	return this.http.post(this.mealUrl, this.filterData(data)).do((x)=>this.dataChange$.next(1))
  }

  editMeal(_id, data) {
  	return this.http.put(`${this.mealUrl}/${_id}`, this.filterData(data)).do((x)=>this.dataChange$.next(1))
  }

  deleteMeal(_id) {
  	return this.http.delete(`${this.mealUrl}/${_id}`).do((x)=>this.dataChange$.next(1))
  }

  private filterData(data) {
    console.log('filtering', data)
    return {
      datetime: data.datetime,
      name: data.name,
      calories: data.calories,
      text: data.text,
      user: data.user ? data.user.id ? data.user.id : data.user : null
    }
  }

  fetchMeals(params) {
  Object.keys(params).forEach((key) => (params[key] == null) && delete params[key]);
    if(params.startDate && params.startDate.toJSON)
      params.startDate = params.startDate.toJSON()
    if(params.endDate && params.endDate.toJSON)
      params.endDate =  params.endDate.toJSON()
  	return this.http.get<MealListResponse>(`${this.mealUrl}`, {
      params,
      observe: 'body'
    }).map((x)=>{
      return x;
    })
  }
}

interface MealListResponse {
  data: Meal[];
  size: number;
}
