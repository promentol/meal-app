import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../user.model'
import { UserDialogComponent } from '../user-dialog/user-dialog.component';
import { UserService } from '../user.service'
import * as swal from 'sweetalert'

declare function swal(a:any, b:any, c:any); 

import { 
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSnackBar
} from '@angular/material';

@Component({
  selector: 'user-actions',
  templateUrl: './user-actions.component.html',
  styleUrls: ['./user-actions.component.css']
})
export class UserActionsComponent implements OnInit {
  
  @Input()
  user: User;

  @Output() dataUpdate: EventEmitter<any> = new EventEmitter();

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  edit(user) {
    let dialogRef = this.dialog.open(UserDialogComponent, {
      //: '400px',
      data: { 
        type: 'edit',
        user
      }
    });

    return dialogRef.afterClosed().subscribe(()=>{
      this.dataUpdate.emit()
    })
  }

  remove(user) {

    swal("Are you sure?", 'Your will not be able to recover this user!',{
      dangerMode: true,
      buttons: true,
    }).then((x)=>{
      this.userService.deleteUser(user.id).subscribe(()=>{
        this.dataUpdate.emit()
        this.snackBar.open('User is deleted', '', {
          duration: 3000
        })
      })
    })
  }

}
