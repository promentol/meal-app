import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
//import { MealModule } from './meal/meal.module';
//import { UserModule } from './user/user.module';
import { LayoutComponent } from './layout/layout.component';
import { CoreModule } from '../core/core.module'

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    //UserModule,
    //MealModule,
    LayoutRoutingModule
  ],
  declarations: [LayoutComponent]
})
export class LayoutModule { }