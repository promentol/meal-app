import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from '../../core/core.module';
import { UserService } from './user.service';
import { UserListComponent } from './user-list/user-list.component';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { UserActionsComponent } from './user-actions/user-actions.component';
import { UserFilterComponent } from './user-list/user-filter/user-filter.component';

const routes: Routes = [
    { path: '', component: UserListComponent, data: { title: 'List of Users' } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule
  ],
  entryComponents: [UserDialogComponent],
  declarations: [UserListComponent, UserDialogComponent, UserActionsComponent, UserFilterComponent],
  providers: [UserService]
})
export class UserModule { }
