import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core/user.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  signUpForm: FormGroup;
  constructor(
    private fb: FormBuilder, 
    private userService: UserService,
    private toasterService: ToasterService,
    private router: Router,
  ) {
  	this.signUpForm = this.fb.group({
      email: ['', [Validators.required,  Validators.email]],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
      name: ['', Validators.required],
    });
  }

  onSubmit() {
  	if(this.signUpForm.valid) {
      if(this.signUpForm.value.password != this.signUpForm.value.passwordConfirm){
        this.toasterService.pop('error', 'Error', 'Passwords are not matching');
      } else {
        this.userService.signUp(this.signUpForm.value).subscribe((e) => {
          this.toasterService.pop('success', 'Check Email', 'Please check your email for verification');
          this.router.navigate(['login']);
        })
      }
  	}
  }

  ngOnInit() {
  }

}
