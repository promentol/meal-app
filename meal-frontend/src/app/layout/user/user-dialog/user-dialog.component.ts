import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service'
import * as CoreUserService from '../../../core/user.service'

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css']
})
export class UserDialogComponent implements OnInit {
  
  userForm: FormGroup;
  constructor(
  	public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _fb: FormBuilder,
    private userService: UserService,
    private selfUserService: CoreUserService.UserService
  ) { }

  ngOnInit() {
    if(!this.data.user) {
      this.data.user = {}
    }
    this.userForm = this._fb.group({
        name: [
          this.data.user.name,
          [Validators.required, Validators.minLength(3), Validators.maxLength(255)]
        ],
        role: [this.data.user.role],
        email: [
          this.data.user.email, [
            Validators.required,
            Validators.email
        ]],
        password: [this.data.user.password, this.data.type=='add'?Validators.required:null],
        verified: [this.data.user.verified, Validators.required]
    });
  }

  save() {
    if(this.userForm.valid){
      if(this.data.type == 'add') {
        this.userService.addUser(this.userForm.value).subscribe(()=>this.dialogRef.close())
      } else {
        this.userService.editUser(this.data.user.id, this.userForm.value).subscribe(()=>this.dialogRef.close())
      }
    }
  }

  onNoClick() {
	this.dialogRef.close()
  }

}
