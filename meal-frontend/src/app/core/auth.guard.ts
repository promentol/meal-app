import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service'

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(
	    private router: Router,
	    private userService: UserService
    ){

	}
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
    	return this.userService.isLogged.map((x) => {
            console.log(x, 'isLogged')
            if(!x) {
                this.router.navigate(['login']);
            }
            return !!x
        });
    }
}
