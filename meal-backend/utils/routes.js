exports.getPasswordRecoveryUrl = function(x){
    return exports.makeUrl(`/password/${x}`)
}

exports.makeUrl = function(x){
    return process.env.FRONT_URL+ x;
}

exports.getVerifyUrl = function(x){
	return exports.makeUrl(`/verify/${x}`)
}

module.exports = exports;
