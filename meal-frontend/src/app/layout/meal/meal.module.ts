import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MealListComponent } from './meal-list/meal-list.component';
import { CoreModule } from '../../core/core.module';
import { MealDialogComponent } from './meal-dialog/meal-dialog.component';
import { FormsModule } from '@angular/forms';

import { MealService } from './meal.service';
import { CaloriesService } from './calories.service';
import { UserService } from './user.service';
import { MealActionsComponent } from './meal-actions/meal-actions.component';
import { MealTableComponent } from './meal-table/meal-table.component';
import { MealFilterComponent } from './meal-list/meal-filter/meal-filter.component';
import { CaloriesDisplayComponent } from './meal-list/calories-display/calories-display.component';
import { CaloriesDialogComponent } from './calories-dialog/calories-dialog.component'

const routes: Routes = [
    { path: '', component: MealListComponent, data: { title: 'List of Meals' } }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    CoreModule,
    FormsModule
  ],
  entryComponents: [MealDialogComponent, CaloriesDialogComponent],
  declarations: [MealListComponent, MealDialogComponent, MealActionsComponent, MealTableComponent, MealFilterComponent, CaloriesDisplayComponent, CaloriesDialogComponent],
  providers: [MealService, CaloriesService, UserService]
})
export class MealModule { }
