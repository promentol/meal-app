import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MealService } from '../meal.service'
import { 
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSnackBar,
  MatPaginator
} from '@angular/material';
import { MealDialogComponent } from '../meal-dialog/meal-dialog.component';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Subject } from 'rxjs/Subject'
import { Subscription } from 'rxjs/Subscription'

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/combineLatest';
import { DataSource } from '@angular/cdk/collections';
import { Meal } from '../meal.model'
import * as CoreUserService from '../../../core/user.service'

@Component({
  selector: 'app-meal-list',
  templateUrl: './meal-list.component.html',
  styleUrls: ['./meal-list.component.css']
})
export class MealListComponent implements OnInit {
  displayedColumns = ['id', 'name', 'date', 'calories', 'action'];
  displayedColumnsAdmin = ['id', 'user', 'name', 'date', 'calories', 'action'];
  dataSource: MealDataSource;
  public pageConfig =  {
    size: 10,
    options: [5, 10, 20]
  }

  @ViewChild(MatPaginator) paginator: MatPaginator
  constructor(
    private mealService: MealService,
    private dialog: MatDialog,
    private selfUserService: CoreUserService.UserService
  ) {
  }

  openModal(type, meal) {
    let dialogRef = this.dialog.open(MealDialogComponent, {
      //width: '400px',
      data: { type, meal }
    });

    dialogRef.afterClosed().subscribe(()=>{
      this.dataSource.refresh({})
    })
  }
  obs: Subscription;
  showUserInput: boolean;

  ngOnInit() {
    this.dataSource = new MealDataSource(this.mealService);
    this.obs = this.selfUserService.isLogged.subscribe((x)=>{
      if(x && x.role == 'admin') {
        this.displayedColumns = this.displayedColumnsAdmin
        this.showUserInput = true
      }
    })
  }

  ngOnDestroy() {
    this.obs.unsubscribe()
  }

  refreshTable({}) {
    this.dataSource.refresh({})
  }

  filter(val){
    this.dataSource.refresh({
      ...val,
      start: 0
    })
    this.paginator.pageIndex = 0
  }

  updatePage(x){
    this.dataSource.refresh({
      start: x.pageIndex*x.pageSize,
      end: x.pageSize
    })
  }
}


export class MealDataSource extends DataSource<Meal> {
  parameters: any;
  constructor(private mealService: MealService) {
    super();
  }
  refresh(x){
    this.options.next({
      ...this.options.getValue(),
      ...x
    })
  }
  size: Subject<number> = new Subject()
  options: BehaviorSubject<any> = new BehaviorSubject({
    start: 0,
    end: 10
  })
  filters: BehaviorSubject<any> = new BehaviorSubject({})
  connect(): Observable<Meal[]> {
    return this.options.switchMap((x)=>{
      return this.mealService.fetchMeals(x).do((z)=>{
         this.size.next(z.size)
      }).map((z)=>{
        return z.data
      })
    })

  }
  disconnect() {}
}