const PasswordRecovery = require('../db/PasswordRecovery')
const {User} = require('../db/User')
const EmailService = require('./EmailService')
const RouteMaker = require('../utils/routes')

exports.recoverPassword = async function (email) {
    console.log(email)
	const user = await User.findOne({email})
    if(!user){
        throw {
            status: 400,
            message: 'User not found'
        }
    }
    const pr = await PasswordRecovery.create({
        createdDate: new Date(),
        status: '0',
        userId: user.id
    })

    setTimeout(async ()=>{
    	EmailService.send({
            email: user.email,
            text: `Dear ${user.name}, please recover your password ${RouteMaker.getPasswordRecoveryUrl(pr.code)}`,
            subject: 'Password Recovery'
        })
    }, 0)


}

exports.changePassword = async function(code, password) {
	var passwordRecoveryRequest = await PasswordRecovery.findOne({
		code: code,
		status: '0'
    })
    if(!passwordRecoveryRequest) {
        throw {
            status: 400,
            message: 'Request not found'
        }
    }
    var user = await User.findOne({
        _id: passwordRecoveryRequest.userId
    })
    user.set({
        password
    })
    await user.save()
    passwordRecoveryRequest.set({
    	usedDate: new Date(),
    	status: '1'
    })
    await passwordRecoveryRequest.save()
    
}

module.exports = exports