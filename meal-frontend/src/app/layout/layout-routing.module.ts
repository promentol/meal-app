import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component'
import { CoreModule } from '../core/core.module';

import { AuthGuard } from '../core/auth.guard';
import { ModeratorGuard } from '../core/moderator.guard';

const routes: Routes = [
	{
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'meals', pathMatch: 'full' },
            { path: 'meals', loadChildren: './meal/meal.module#MealModule' },
            {
              path: 'users',
              loadChildren: './user/user.module#UserModule',
              canActivate: [ModeratorGuard]
            }
        ],
        canActivate: [AuthGuard]
    }
];

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
