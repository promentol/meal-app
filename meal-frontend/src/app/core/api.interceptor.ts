import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ToasterService } from 'angular2-toaster';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

	constructor(
    public toasterService: ToasterService,
    public router: Router
  ){

	}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const dupReq = localStorage.getItem('token') ? req.clone({
      headers: req.headers.set('token', localStorage.getItem('token'))
    }) : req;
    return next.handle(dupReq)
      .catch((err: any, caught) => {
        console.log(err.status)
          if (err instanceof HttpErrorResponse) {
            if(err.status == 403) {
              this.router.navigate(['login']);
            }
            this.toasterService.pop('error', 'error', err.error.message)
          }


          return Observable.throw(err);
      });
  }
}