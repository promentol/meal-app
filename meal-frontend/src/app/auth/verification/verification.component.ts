import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../core/user.service'
import { ToasterService } from 'angular2-toaster';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit, OnDestroy {
	private sub: any;
	error: string;
  	constructor(
  		private route: ActivatedRoute,
  	    private userService: UserService,
	    private router: Router,
	    private toasterService: ToasterService
	) {
	}

	ngOnInit() {
		this.sub = this.route.params.flatMap(params => {
			return this.userService.verify(params['code'])
		}).subscribe(
			result => {
				this.toasterService.pop('success', 'Congratulations', 'you have been verified')
				this.router.navigate(['login']);
			},
	        error => {
	            //this.errors = error
	        }
		);
	}

	ngOnDestroy() {
		this.sub.unsubscribe()
	}

}
