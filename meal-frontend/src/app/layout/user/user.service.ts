import { Injectable } from '@angular/core';
import { User } from './user.model'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserService {

  constructor(public http: HttpClient) {

  }

  userUrl = `${environment.API_URL}/users`

  addUser(data) {
    return this.http.post(this.userUrl, {
      isModerator: true,
      ...data
    })
  }

  editUser(_id, data) {
    return this.http.put(`${this.userUrl}/${_id}`, data)
  }

  deleteUser(_id) {
    return this.http.delete(`${this.userUrl}/${_id}`)
  }

  fetchUsers(params) {
  	return this.http.get<UserListResponse>(`${this.userUrl}`, {
      params,
      observe: 'body'
    }).map((x)=>{
      return x;
    })
  }
}


interface UserListResponse {
  data: User[];
  size: number;
}
