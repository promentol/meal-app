import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../core/user.service'
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnDestroy {

  passwordForm: FormGroup;
  private sub: any;
  constructor(
    public fb: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private toasterService: ToasterService
  ) {
  	this.passwordForm = this.fb.group({
      password: ['', [Validators.required]],
      passwordConfirm: ['', [Validators.required]]
    });
  }

  onSubmit() {
  	if(this.passwordForm.valid) {
      if(this.passwordForm.value.password != this.passwordForm.value.passwordConfirm){
        this.toasterService.pop('error', 'Error', 'Passwords are not matching');
      } else {
    		this.sub = this.route.params.flatMap(params => {
          return this.userService.changePassword(params['code'], this.passwordForm.value)
        }).subscribe(
          result => {
            this.toasterService.pop('success', 'Congratulations', 'Your Password have been changed')
            this.router.navigate(['login']);
          },
          error => {
              //this.errors = error
          }
        );
      }
  	}
  }


  ngOnDestroy() {
    this.sub.unsubscribe()
  }

}
