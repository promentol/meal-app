import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaloriesDisplayComponent } from './calories-display.component';

describe('CaloriesDisplayComponent', () => {
  let component: CaloriesDisplayComponent;
  let fixture: ComponentFixture<CaloriesDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaloriesDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaloriesDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
