import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../core/user.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(
    public fb: FormBuilder,
    private router: Router,
    public userService: UserService
  ) {
  	this.loginForm = this.fb.group({
      email: ['', [Validators.required,  Validators.email]],
      password: ['', Validators.required ]
    });
  }

  onSubmit() {
  	if(this.loginForm.valid) {
  		this.userService.login(this.loginForm.value).subscribe((e) => {
  			this.router.navigate(['/']);
  		})
  	}
  }

  ngOnInit() {
  }

}
