const Router = require('koa-router');
const router = new Router({
	prefix: '/auth'
});

const AuthService = require('../services/AuthService')
const auth = require('../middleware/auth')

router.get('/', auth(), async function (ctx, next) {
	ctx.body = await AuthService.logIn(ctx.user)
})

router.post('/', async function (ctx, next) {
	let {
		request: {
			body: {
				email,
				password
			}
		}
	} = ctx;
	if(email) {
		email = email.toString()
	}
	if(password) {
		password = password.toString()
	}
	const auth = await AuthService.loginWithPassword(email, password)
	ctx.body = auth;
})

module.exports = router.routes()