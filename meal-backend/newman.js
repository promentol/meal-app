var newman = require('newman');

newman.run({
    collection: require('./test/userFlow.postman_collection.json'),
    environment: require('./test/localTest.postman_environment.json'),
    reporters: 'cli'
}, function (err) {
	if (err) { throw err; }
    console.log('collection run complete!');
});