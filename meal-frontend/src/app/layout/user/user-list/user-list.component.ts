import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../user.model'
import { UserService } from '../user.service'
import { Observable } from 'rxjs/Observable'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Subject } from 'rxjs/Subject'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/observable/combineLatest'
import { DataSource } from '@angular/cdk/collections';
import { 
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSnackBar,
  MatPaginator
} from '@angular/material'

import {UserDialogComponent} from '../user-dialog/user-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  displayedColumns = ['id', 'name', 'role', 'email', 'verified', 'action'];
  dataSource: UserDataSource;
  public pageConfig =  {
    size: 10,
    options: [5, 10, 20]
  }
  @ViewChild(MatPaginator) paginator: MatPaginator
  constructor(
    public userService: UserService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.dataSource = new UserDataSource(this.userService);
    this.paginator.pageIndex = 0
  }
  refreshTable(){
    this.dataSource.refresh({})
  }

  filter(val){
    
    this.dataSource.refresh({
      ...val,
      start: 0
    })
    this.paginator.pageIndex = 0
  }
  openModal(type, user){
    let dialogRef = this.dialog.open(UserDialogComponent, {
      //width: '400px',
      data: { type, user }
    });

    dialogRef.afterClosed().subscribe(()=>{
      this.dataSource.refresh({})
    })
  }
  updatePage(x){
    this.dataSource.refresh({
      start: x.pageIndex*x.pageSize,
      end: x.pageSize
    })
  }

}


export class UserDataSource extends DataSource<User> {
  parameters: any;
  constructor(private userService: UserService) {
    super();
  }
  refresh(x){
    this.options.next({
      ...this.options.getValue(),
      ...x
    })
  }
  size: BehaviorSubject<number> = new BehaviorSubject(0)
  options: BehaviorSubject<any> = new BehaviorSubject({
    start: 0,
    end: 10
  })
  connect(): Observable<User[]> {
    return this.options.switchMap((x)=>{
      return this.userService.fetchUsers(x).do((z)=>{
         this.size.next(z.size)
      }).map((z)=>{
        return z.data
      })
    })
  }
  disconnect() {}
}