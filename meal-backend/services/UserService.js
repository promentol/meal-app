const {User, ROLES} = require('../db/User')
const RouteMaker = require('../utils/routes')
const EmailService = require('./EmailService')

exports.addUser = async function(data) {
    try {
        const user = await User.create(data)
    } catch(e) {
        if(e.code == 11000){
            throw {
                status: 400,
                message: "user with this email exists"
            }
        } else {
            throw e
        }
    }
    //TODO check email
    if(!data.verified){
        setTimeout(async () => {
            EmailService.send({
                email: user.email,
                text: `Dear ${data.name}, please verify your account with this link ${RouteMaker.getVerifyUrl(user.verification_code)}`,
                subject: 'Verify Email'
            })
        }, 0)
    }
    return user
}

exports.verify = async function(verification_code) {
    const user = await User.findOne({verification_code})
    if(!user) {
        throw ({
            status: 400,
            message: 'User not exist'
        })
    }
    if(user.verified) {
        throw ({
            status: 400,
            message: 'User is verified'
        })
    }
    user.set({
        verified: true
    })
    await user.save()
    return user
}

exports.editUser = async function(user, _id, data) {
    const finalUser = await User.findById(_id)
    if(user._id.toString() == _id){
        if(data.verified || data.role){ 
            if( (data.role && user.role != data.role) 
                || (data.verified && user.verified != data.verified)) {
                throw ({
                    status: 400,
                    message: 'You Can not change your role or verified status'
                })
            }
        }
    }

    if(user.isSimple()){
        delete user.verified
        delete user.role
        if(user._id.toString() != finalUser._id.toString()) {
            throw {
                status: 403,
                message: 'Invalid Action'
            }
        }
    }

    if(user.isModerator() && !user.isAdmin()){
        delete user.role
        if(!finalUser.isSimple() && user._id.toString() != finalUser._id.toString()) {
            throw ({
                status: 403,
                message: 'Invalid Action'
            })
        }
    }

    finalUser.set(data)

     await finalUser.save()
     return finalUser
}

exports.deleteUser = async function(user, _id) {
    const finalUser = await User.findById(_id)
    if(user.isSimple()){
        throw {
            status: 403,
            message: 'Invalid Action'
        }
    }
    if(user._id.toString() == _id){
        throw {
            status: 400,
            message: 'You Can not delete yourself'
        }
    }
    if(user.isModerator() && !user.isAdmin()){
        if(!finalUser.isSimple()) {
            throw {
                status: 403,
                message: 'Invalid Action'
            }
        }
    }
    return finalUser.remove()
}

exports.getUsers = async function(user, options) {
    const condition = {}

    if(options.q){
        condition.$or = [{
            name: {
                $regex: new RegExp(options.q, "i")

            }
        }]
    }

    if(!user.isAdmin()){
        condition.role = ROLES.USER
    }
    console.log(condition)
    return {
        size: await User.count(condition),
        data: await User.find(condition).skip(parseInt(options.start) || 0).limit(parseInt(options.end) || 10)
    }
}

module.exports = exports