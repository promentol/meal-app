import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild} from '@angular/core';
import { UserService } from '../../core/user.service'
import { ActivatedRoute, Router } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;
  constructor(
  	private router: Router,
	public userService: UserService
  ) {

  }

  user: Observable<any>;
  obs: Subscription;

  ngOnInit() {
    this.obs = this.userService.isLogged.subscribe((x)=>{
      this.user = x
    })
  }

  ngOnDestroy() {
    this.obs.unsubscribe()
  }

  close() {
    this.sidenav.close()
  }

  logout() {
  	this.userService.logout()//.subscribe(()=>{
  		this.router.navigate(['/login']);
  	//})
  	
  }

}
