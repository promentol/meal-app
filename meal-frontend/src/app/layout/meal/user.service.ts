import { Injectable } from '@angular/core';
import { User } from './user.model'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserService {

  constructor(public http: HttpClient) {

  }

  userUrl = `${environment.API_URL}/users`

  fetch(params) {
  	return this.http.get<UserListResponse>(`${this.userUrl}`, {
      params,
      observe: 'body'
    }).map((x)=>{
      return x.data;
    })
  }
}


interface UserListResponse {
  data: User[];
  size: number;
}
