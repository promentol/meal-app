const Router = require('koa-router');
const router = new Router({
	prefix: '/meals'
});

const MealService = require('../services/MealService')
const auth = require('../middleware/auth')

router.get('/', async function (ctx, next) {
	ctx.body = await MealService.getMeals(ctx.user,ctx.request.query)
})

router.get('/summary', async function(ctx, next){
	const data = await MealService.getCaloriesPerDay(
		ctx.user._id,
		ctx.query.startDate,
		ctx.query.endDate,
		parseInt(ctx.query.start) || 0,
		parseInt(ctx.query.end) || 10
	)

	data.size = data.size[0] ? data.size[0].count : 0

	ctx.body = data
})

router.post('/', async function (ctx, next) {
	const meal = await MealService.addMeal(ctx.user, {
		name: ctx.request.body.name,
		datetime: ctx.request.body.datetime,
		text: ctx.request.body.text,
		calories: ctx.request.body.calories,
		user: ctx.user.isAdmin() ? ctx.request.body.user : ctx.user._id
	})
    ctx.body = meal;
})

router.get('/:id', async function(ctx, next) {
	const meal = await MealService.getMeal(ctx.user, ctx.params.id);
	ctx.body = meal
})

router.put('/:id', async function (ctx, next) {
	const meal = await MealService.editMeal(ctx.user, ctx.params.id, {
		name: ctx.request.body.name,
		datetime: ctx.request.body.datetime,
		text: ctx.request.body.text,
		calories: ctx.request.body.calories,
		user: ctx.request.body.user || ctx.user._id
	})
    ctx.body = meal;
})

router.delete('/:id', async function (ctx, next) {
	await MealService.deleteMeal(ctx.user, ctx.params.id)
    ctx.body = {};
})

module.exports = router.routes()