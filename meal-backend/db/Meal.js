const mongoose = require('mongoose');

const miniUserSchema = mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    name: {
        type: String,
        required: true
    }
})

const mealSchema = mongoose.Schema({
    name: {
        type: String,
        required : true
    },
    date: {
        type: Date,
        required : true
    },
    time: {
        type: Number,
        required : true
    },
    text: {
        type: String,
        required : true
    },
    calories: {
        type: Number,
        required : true
    },
    user: {
        type: miniUserSchema,
        required: true
    }
}, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
});

mealSchema.virtual('datetime').
    get(function() {
        if(this.date){
            var v = new Date(this.date);

            v.setHours(
                Math.floor(this.time/60),
                this.time%60
            )
            return v
        }
        return
    }).
    set(function(v) {
        if(v){
            this.date = new Date(v)
            this.time = new Date(0)
            console.log(v, this.date)
            this.time = this.date.getHours()*60+this.date.getMinutes()

            this.date.setHours(0,0,0,0)
        }
      })


const Meal = mongoose.model('Meal', mealSchema);
mongoose.Meal = Meal

mealSchema.options.toJSON = {
    transform: function(doc, ret, options) {
        ret.datetime = doc.datetime
        ret.id = ret._id
        delete ret.date;
        delete ret.__v;
        delete ret.time;
        return ret;
    }
}

module.exports = Meal