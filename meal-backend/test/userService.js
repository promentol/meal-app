var assert = require('assert');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URL);
const UserService = require('../services/UserService')
const {User, ROLES} = require('../db/User')

describe('User', function() {

	this.timeout(10000);

	before(async function() {
		this.Admin = await User.create({
			name: 'TestAdmin',
			password: 'asd',
			email: 'admin@asd.asd',
			role: ROLES.ADMIN
		})
		console.log('asd - 1')
		this.Moderator = await User.create({
			name: 'TestModerator',
			password: 'asd',
			email: 'moderator@asd.asd',
			role: ROLES.MODERATOR
		})
		console.log('asd - 2')
		this.UserA = await User.create({
			name: 'TestUserA',
			password: 'asd',
			email: 'userA@asd.asd'
		})
		console.log('asd - 3')
		this.UserB = await User.create({
			name: 'TestUserB',
			password: 'asd',
			email: 'userB@asd.asd'
		})
		console.log('asd - 4')
	});

	after(async function() {
		await User.remove({
			name: {
				$in: ['TestAdmin', 'TestModerator', 'TestUserA', 'TestUserB', 'AnotherUser']
			}
		})

		console.log('asd - finish')
		mongoose.connection.close()
	});


  describe('ADD USER', function() {

    it('Should be invalid to add user with existing email ', async function() {
      
      	var wasInCatch = false
    	try{
	      await UserService.addUser({
	      	name: 'AnotherUser',
	      	password: 'asd',
	      	email: 'userB@asd.asd'
	      })
    	} catch(e) {
    		wasInCatch = true
    		assert.equal(e.status, 400)
    		assert.equal(e.message, "user with this email exists")
    	}
    	assert.equal(wasInCatch, true)
    });
  });

  describe('EDIT USER', function() {

    it('Should be invalid to modify admin as moderator', async function() {
      	var wasInCatch = false
    	try {
    		await UserService.editUser(this.Moderator, this.Admin._id, {
    			name: 'AnotherAdmin'
    		})
    	} catch(e) {
    		wasInCatch = true
    		assert.equal(e.message, 'Invalid Action')
    	}
    	assert.equal(wasInCatch, true)
    	const NotModifiedAdmin = await User.findById(this.Admin._id)
    	assert.equal(this.Admin.name, NotModifiedAdmin.name)

    });
    it('Should be invalid to modify user as another user', async function() {
      	var wasInCatch = false
    	try {
    		await UserService.editUser(this.UserA, this.UserB._id, {
    			name: 'AnotherUser'
    		})
    	} catch(e) {
    		wasInCatch = true
    		assert.equal(e.message, 'Invalid Action')
    	}
    	assert.equal(wasInCatch, true)
    	const NotModifiedUserData = await User.findById(this.UserB._id)
    	assert.equal(this.UserB.name, NotModifiedUserData.name)
    });
    it('Should change the user\'s name as Admin', async function() {

		await UserService.editUser(this.Admin, this.UserA._id, {
			name: 'AnotherUser',
			role: ROLES.MODERATOR
		})
    	const UserA = await User.findById(this.UserA._id)
    	assert.equal(UserA.name, 'AnotherUser')
    });

  })

  describe('DELETE USER', function() {

    it('Should be invalid to remvoe Admin as Moderator', async function() {
    	var wasInCatch = false
    	try {
    		await UserService.deleteUser(this.Moderator, this.Admin._id)
    	} catch(e) {
    		wasInCatch = true
    		assert.equal(e.message, 'Invalid Action')
    	}
    	assert.equal(wasInCatch, true)
    	const NotModifiedAdmin = await User.findById(this.Admin._id)
    	assert.equal(NotModifiedAdmin._id.toString(), this.Admin._id.toString())

    });
    it('Should be invalid to remvoe User as User', async function() {
      	var wasInCatch = false
    	try {
    		await UserService.deleteUser(this.UserA, this.UserB._id)
    	} catch(e) {
    		wasInCatch = true
    		assert.equal(e.message, 'Invalid Action')
    	}
    	assert.equal(wasInCatch, true)
    	const NotModifiedUserData = await User.findById(this.UserB._id)
    	//Check if NotModifiedUserData exists
    	assert.equal(this.UserB._id.toString(), NotModifiedUserData._id.toString())
    });

    it('Should remove the user', async function() {
      await UserService.deleteUser(this.Admin, this.UserB._id)
  	  const UserB = await User.findById(this.UserB._id)
  	  assert.equal(UserB, null)
    });
  })
});