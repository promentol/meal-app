const Router = require('koa-router');
const router = new Router({
	prefix: '/password-recovery'
});

const auth = require('../middleware/auth')

const UserService = require('../services/UserService')
const PasswordService = require('../services/PasswordService')

router.post('/', async function (ctx, next) {
	//ctx.body='user'
	if(ctx.request.body.email) {
		ctx.request.body.email = ctx.request.body.email.toString()
	}
	await PasswordService.recoverPassword(ctx.request.body.email || "")
    //const auth = AuthService.logIn(user)
    ctx.body = {};
})

router.post('/:code', async function (ctx, next) {
	//ctx.body='user'
	if(ctx.params.code) {
		ctx.params.code = ctx.params.code.toString()
	}
    await PasswordService.changePassword(ctx.params.code, ctx.request.body.password)
    ctx.body = {};
})

module.exports = router.routes()