import { Component, OnInit } from '@angular/core';
import { CaloriesService } from '../../calories.service'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'

import { 
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSnackBar
} from '@angular/material';

import { CaloriesDialogComponent } from '../../calories-dialog/calories-dialog.component';

@Component({
  selector: 'calories-display',
  templateUrl: './calories-display.component.html',
  styleUrls: ['./calories-display.component.css']
})
export class CaloriesDisplayComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    public caloriesService: CaloriesService
  ) { }
  isDanger: BehaviorSubject<boolean>= new BehaviorSubject(false);

  ngOnInit() {

  	Observable
  		.combineLatest(this.caloriesService.expectedCalories, this.caloriesService.calories)
  		.map(([x, y])=>x<y)
  		.subscribe(this.isDanger)
      console.log(this.caloriesService.expectedCalories)
      this.caloriesService.expectedCalories.do((x)=>console.log(x)).subscribe((x)=>{
        console.log(x)
      })


  }

  edit() {
    let dialogRef = this.dialog.open(CaloriesDialogComponent);
    return dialogRef.afterClosed()
  }

}
