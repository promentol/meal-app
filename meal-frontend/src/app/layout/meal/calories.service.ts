import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/distinct';
import 'rxjs/add/operator/merge';
import 'rxjs/observable/interval';
import 'rxjs/observable/combineLatest';

import { UserService } from '../../core/user.service'
import { MealService } from './meal.service'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable()
export class CaloriesService {


  v = Math.random();
  constructor(
  	private userService: UserService,
  	private mealService: MealService,
    private http: HttpClient
  ) {
  	this.mealService.dataChange$
      .switchMap(()=>this._fetchCalories({
        startDate: this.currentDate.getValue(),
        endDate: this.currentDate.getValue()
      }))
      .map((x)=>x && x.data.length?x.data[0].totalCalories : 0)
      .subscribe(this.calories)

	  this.currentDate
      .distinct()
      .switchMap((date)=>this._fetchCalories({
        startDate: date,
        endDate: date
      }))
      .map((x)=>x && x.data.length?x.data[0].totalCalories : 0)
  		.subscribe(this.calories)

  	this.userService.user.do((x)=>console.log(x)).map((user)=>user && user.expectedCalories).subscribe(this.expectedCalories)

    this.expectedCalories.subscribe(
      val => console.log('val: ' + val),
      err => console.log('err: ' + err),
      () => console.log('stream completed')
    )    //Demonstration of interval and distinct, not for production
    //Observable.interval(1000).map(()=>this.getBlankDate()).subsribe(this.currentDate);
    //Use also delay for date change
  }

  currentDate: BehaviorSubject<Date> = new BehaviorSubject(this.getBlankDate());
  
  expectedCalories: BehaviorSubject<number> = new BehaviorSubject(0);
  calories: BehaviorSubject<number> = new BehaviorSubject(0);

  changeExpectedCalories(expectedCalories) {
  	return this.userService.update({
  		expectedCalories
  	})
  }

  getCaloriesSummary(params) {
    return this._fetchCalories(params).merge(this.mealService.dataChange$.switchMap(()=>this._fetchCalories(params)))
  }

  caloriesUrl = `${environment.API_URL}/meals/summary`;
  private _fetchCalories({
    startDate,
    endDate, 
   ...params
   }) {
    console.log(startDate, endDate, 'limit')
  	return this.http.get<Summary>(this.caloriesUrl, {
      params: {
        ...params,
        startDate: startDate.toJSON(),
        endDate: endDate.toJSON(),
      }
    })
  }

  private getBlankDate(){
    var v =  new Date()
    v.setHours(0,0,0,0)
    return v
  }

}

interface Summary {
  size: number;
  data: {
    _id: Date;
    totalCalories: number;
  }[]
}
