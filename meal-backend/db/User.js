const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const shortid = require('shortid')
const validator = require('validator');

const ROLES = {
    USER: 'user',
    ADMIN: 'admin',
    MODERATOR: 'moderator'
}

////add validation to user email uniqueness


const userSchema = mongoose.Schema({
    name: {
    	type: String,
    	required : true
    },
    email: {
    	type: String,
    	unique : true,
    	required : true,
        validate: {
          validator: function(v) {
            return validator.isEmail(v)
          },
          message: '{VALUE} is not a valid email!'
        }
    },
    password_hash: {
    	type: String,
    	required : true
    },
    role: {
    	type: String,
    	required : true,
        enum: [ROLES.USER, ROLES.ADMIN, ROLES.MODERATOR],
    	default: ROLES.USER
    },
    verification_code: {
        type: String,
        required : true,
        default: function() {
            return shortid.generate()
        }
    },
    verified: {
    	type: Boolean,
    	required : true,
    	default: false
    },
    expectedCalories: {
        type: Number,
        required : true,
        default: 1,
        min: [1, 'Few calories'],
        max: [1000000000, 'Too much calories']

    }
});

userSchema.index({
    name: 'text',
    email: 'text'
}, {
    name: 'search index',
    weights: {
        name: 3,
        email: 1
    }
});

userSchema.options.toJSON = {
    transform: function(doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.password_hash;
        delete ret.verification_code;
        return ret;
    }
}

userSchema.virtual('password').
  set(function(v) {
    if(!v) {
        throw {
            status: 400,
            message: 'Password can not be empty'
        }
    }
  	var salt = bcrypt.genSaltSync(saltRounds)
	var hash = bcrypt.hashSync(v, salt)
	this.password_hash = hash
  });


userSchema.method('deNormalize', function () {
  return {
    id: this.id,
    name: this.name
  }
})

userSchema.method('isModerator', function () {
  return this.role == ROLES.MODERATOR || this.role == ROLES.ADMIN
})

userSchema.method('isAdmin', function () {
  return this.role == ROLES.ADMIN
})

userSchema.method('isSimple', function () {
  return this.role == ROLES.USER
})



userSchema.post('save', function(doc, next) {
    if(mongoose.Meal){
        mongoose.Meal.update({
            "user.id": doc._id
        }, {
            $set: {
                user: doc.deNormalize()
            }
        }).then(function(){
            next()
        })
    } else {
        next()
    }
});

userSchema.post('remove', function(doc, next) {
    if(mongoose.Meal){

        mongoose.Meal.remove({
            "user.id": doc._id
        }).then(function(){
            next()
        })
    } else {
        next()
    }
});

const User = mongoose.model('User', userSchema);
mongoose.User = User


module.exports = {
    User,
    ROLES
}