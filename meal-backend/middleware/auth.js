const AuthService = require('../services/AuthService')

module.exports  = function(params){
	params = params || {}
	return async function(ctx, next) {
		try{
			ctx.user = await AuthService.getUser(ctx.header.token)
		} catch (e){
			throw ({
				status: 403,
				message: 'User Not Found'
			})
		}
		if(params.isModerator && !ctx.user.isModerator()) {
			throw ({
				status: 401,
				message: 'No Valid Action'
			})
		}
		if(params.isAdmin && !ctx.user.isAdmin()) {
			throw ({
				status: 401,
				message: 'No Valid Action'
			})
		}
		if(ctx.user && ctx.user.verified == false) {
			throw ({
				status: 403,
				message: 'User is not verified'
			})
		}
		await next();
	}
}