import { Component, OnInit, ViewChild } from '@angular/core';
import { CaloriesService } from '../calories.service'
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Subject } from 'rxjs/Subject'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/combineLatest';
import { DataSource } from '@angular/cdk/collections';
import {
  MatPaginator
} from '@angular/material';

@Component({
  selector: 'meal-table',
  templateUrl: './meal-table.component.html',
  styleUrls: ['./meal-table.component.css']
})
export class MealTableComponent implements OnInit {
  displayedColumns = ['date', 'calories'];
  public pageConfig =  {
    size: 10,
    options: [5, 10, 20]
  }
  @ViewChild(MatPaginator) paginator: MatPaginator

  constructor(
  	public caloriesService: CaloriesService
  ) { }
  dataSource: CaloriesDataSource;

  ngOnInit() {
    this.dataSource = new CaloriesDataSource(this.caloriesService);
  }

  refreshTable({}) {
    this.dataSource.refresh({})
  }

  startDate: Date;
  endDate: Date;

  filterDate() {
    this.dataSource.refresh({
      startDate: this.resetDate(this.startDate || new Date('2017')),
      endDate: this.resetDate(this.endDate || new Date()) 
    })
  }

  resetDates() {
    this.startDate = null;
    this.endDate = null
    this.dataSource.refresh({
      startDate: new Date('2017'),
      endDate: new Date()
    })
  }

  resetDate(d) {
    d.setHours(0, 0, 0, 0)
    return d
  }

  filter(val){
    this.dataSource.refresh({
      ...val,
      start: 0
    })
    this.paginator.pageIndex = 0
  }

  updatePage(x){
    this.dataSource.refresh({
      start: x.pageIndex*x.pageSize,
      end: x.pageSize
    })
  }

}

export class CaloriesDataSource extends DataSource<any> {
  parameters: any;
  constructor(private caloriesService: CaloriesService) {
    super();
  }
  refresh(x){
    this.options.next({
      ...this.options.getValue(),
      ...x
    })
  }
  size: Subject<number> = new Subject()
  options: BehaviorSubject<any> = new BehaviorSubject({
    start: 0,
    end: 10,
    startDate: new Date('2017'),
    endDate: new Date()
  })
  filters: BehaviorSubject<any> = new BehaviorSubject({})
  connect(): Observable<any> {
    return this.options.switchMap((x)=>{
      return this.caloriesService.getCaloriesSummary({
      	...x,
      }).do((z)=>{
        console.log(z, 'asda  as as')
         this.size.next(z.size)
      }).map((z)=>{
        return z.data
      })
    })

  }
  disconnect() {}
}