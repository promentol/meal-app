import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service'

@Injectable()
export class ModeratorGuard implements CanActivate {
  	constructor(
	    private router: Router,
	    private userService: UserService
    ){

	}

	canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.userService.isModerator.map((x) => {
        console.log(x)
        if(!x) {
            this.router.navigate(['login']);
        }
        return !!x
    });
  }
}
