const Meal = require('../db/Meal')
const {User} = require('../db/User')

const mongoose = require('mongoose')
mongoose.set('debug', true)
exports.addMeal = async function(user, data) {
	var mealUser;
	if(user.isAdmin()){
		mealUser = await this.getUser(data.user)
	} else {
		mealUser = user
	}
	console.log(mealUser.deNormalize())
	data.user = mealUser.deNormalize()
    const meal = await Meal.create(data)
    return meal
}

exports.getUser = async function(id) {
	return await User.findById(id)
}

exports.getCaloriesPerDay = async function(userId, startDate, endDate, skip, limit) {
	const condition = {
    	$match: {
            'user.id': mongoose.Types.ObjectId(userId),
            date: {
            	$gte: new Date(startDate),
            	$lte: new Date(endDate)
            }
        }
    }
    const groupDate =  {
		$group: {
        	_id: "$date",
			totalCalories: { $sum: "$calories" }
		}
	}
    const groupCount =  {
    	$group: { 
    		_id: 1, count: { $sum: 1 }
    	} 
	}
	const skipCount = {
		$skip: skip
	}
	const limitCount = {
		$limit: limit
	}
	const sort = {
		$sort: {
			_id: -1
		}
	}
	return {
		size: await Meal.aggregate([condition, groupDate, groupCount]),
		data: await Meal.aggregate([condition, groupDate, sort, skipCount, limitCount])
	}
    /*Meal.find({
        'user.id': mongoose.Types.ObjectId(userId),
        date: new Date(date)
	});*/
}
exports.getMeal = async function(user, _id, data) {
	const meal = await Meal.findById(_id)
	if(!user.isAdmin() && meal.user.id.toString() !=user._id.toString()){
		throw {
			status: 403,
			message: "Invalid Action"
		}
	}
	return meal
} 

exports.editMeal = async function(user, _id, data) {
	const meal = await Meal.findById(_id)
	if(!user.isAdmin() && meal.user.id.toString() != user._id.toString()){
		throw ({
			status: 403,
			message: "Invalid Action"
		})
	}
	console.log(data)
	if(!user.isAdmin()) {
		delete data.user
	}
	console.log(data.user, 'before')
	if(data.user) {
		data.user = await exports.getUser(data.user)
		data.user = data.user.deNormalize()
	}
	console.log(data.user, 'after')
	meal.set(data)
	return meal.save()
}

exports.deleteMeal =  async function(user, _id) {
	const meal = await Meal.findById(_id)
	if(!user.isAdmin() && meal.user.id.toString() != user._id.toString()){
		throw ({
			status: 403,
			message: "Invalid Action"
		})
	}
	return meal.remove()
}

exports.getMeals = async function(user, options) {
	const condition = {}

	if(!user.isAdmin()){
		condition['user.id'] = user._id
	}
    if(options.name){
        condition["user.name"] = {
            $regex: new RegExp(options.name, "i")
        }
    }
	options = options || {}
	if(options.startDate || options.endDate){
		condition.date = {}
		if(options.startDate){
			condition.date.$gte = new Date(options.startDate)
		}
		if(options.endDate){
			condition.date.$lte = new Date(options.endDate)
		}
	} 
	if(options.startTime || options.endTime){
		condition.time = {}
		if(options.startTime) {
			options.startTime = parseInt(options.startTime)
			condition.time.$gte = options.startTime
		}
		if(options.endTime) {
			options.endTime = parseInt(options.endTime)
			condition.time.$lte = options.endTime
			if(options.startTime && options.endTime < options.startTime ){
				delete condition.time
				condition.$or = [
					{
						time: {$gt: options.startTime}
					},
					{
						time: {$lt: options.endTime}
					}
				]

			}
			
		}
	}
	console.log(options, condition.$or)
	return {
		size: await Meal.count(condition),
		data: await Meal.find(condition)
						.sort({
							date: -1
						})
						.sort({
							time: -1
						})
						.skip(parseInt(options.start) || 0)
						.limit(parseInt(options.end) || 10)
	}
}
