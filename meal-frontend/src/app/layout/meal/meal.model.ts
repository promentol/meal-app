export interface Meal {
	_id?: string;
	datetime?: Date;
	name?: string;
	calories?: number;
	text?: string;
}