import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


interface User {
  expectedCalories: number;
  role: string;
}

@Injectable()
export class UserService {

  obs: Observable<any>;
  user: BehaviorSubject<User> = new BehaviorSubject(undefined);

  constructor(public http: HttpClient) {
  	this.obs = Observable.fromEvent(window, 'storage').map(val => !!localStorage.getItem('token'))
  }


  get isLogged() {
    if(!this.user.getValue()) {
      return this._getUser().do((x)=>this.user.next(x)).catch((err) => {
        console.log("Caught Error, continuing")
        //Return an empty Observable which gets collapsed in the output
        return Observable.of(undefined);
      })
    }
    return this.user
  }

  get isModerator() {
    return this.isLogged.map((user: User)=>{
      return user && (user.role == 'moderator' || user.role == 'admin')
    })
  }

  get isAdmin() {
    return this.isLogged.map((user: User)=>{
      return user && (user.role == 'admin')
    })
  }

  private handleLogin({user, token}):User {
    localStorage.setItem('token', token)
    return user
  }

  logout() {
    this.user.next(null)
    localStorage.removeItem('token')
  }

  loginUrl: string = `${environment.API_URL}/auth`;
  private _getUser() {
    return this.http.get(this.loginUrl).map(this.handleLogin)
  }
  login({email, password}):any {
  	return this.http.post(this.loginUrl, {
      email,
      password
    }).map(this.handleLogin).do((x)=>this.user.next(x))
  }
  update(data) {
    return this.http.put<User>(`${this.userUrl}/me`, data).do((x)=>this.user.next(x))
  }

  userUrl: string = `${environment.API_URL}/users`;
  signUp(data):any {
    return this.http.post(this.userUrl, data)//.map(res => res.json())
  }

  verifyUrl: string = `${environment.API_URL}/users/verify`;
  verify(code):any {
    return this.http.post(this.verifyUrl, {
      code
    })
  }

  passwordUrl: string = `${environment.API_URL}/password-recovery`
  requestPassword({email}) {
    return this.http.post(`${this.passwordUrl}`, {
      email
    })
  } 
  changePassword(code, {password}) {
    return this.http.post(`${this.passwordUrl}/${code}`, {
      password
    })
  }

}
