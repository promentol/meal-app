import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MealService } from '../meal.service'
import { UserService } from '../user.service'

import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/distinct';
import 'rxjs/observable/interval';
import 'rxjs/observable/combineLatest';

import * as CoreUserService from '../../../core/user.service'


interface MealModalData {
	type: string;
	meal?: any;
  user?: User;
}

class User {
  name?: string;
  id?: string;
}

function validateUser(c: FormControl) {
  if(!(c.value.id && c.value.name)) {
    return {
      validateUser: false
    }
  }
  return null
}


@Component({
  selector: 'app-meal-dialog',
  templateUrl: './meal-dialog.component.html',
  styleUrls: ['./meal-dialog.component.css']
})
export class MealDialogComponent implements OnInit, OnDestroy {

  mealForm: FormGroup;
  constructor(
  	public dialogRef: MatDialogRef<MealDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MealModalData,
    private _fb: FormBuilder,
    private mealService: MealService,
    private userService: UserService,
    private selfUserService: CoreUserService.UserService

  ) {

  }

  timeNow = new Date(Date.now())
  filteredOptions: Observable<User[]>;
  obs: Subscription
  ngOnInit() {

  	if(!this.data.meal) {
  		this.data.meal = {
        user: {
          id: 'asd'
        }
      }
  	}
  	this.mealForm = this._fb.group({
        name: [
          this.data.meal.name, [
          Validators.required, 
          Validators.minLength(4), 
          Validators.maxLength(25)
        ]],
      	datetime: [
          this.data.meal.datetime,
          Validators.required
        ],
        text: [
          this.data.meal.text,[
          Validators.required,
          Validators.maxLength(255)
          ]
        ],
        user: [
          this.data.meal.user,
          validateUser
        ],
      	calories: [
          this.data.meal.calories, [
          Validators.required,
          Validators.min(1),
          Validators.max(1000000000)
        ]]
    });

    this.obs = this.selfUserService.isLogged.subscribe((user)=>{
      if(user && user.role!='admin'){
        this.mealForm.controls['user'].setValue({
          id: user.id,
          name: user.name
        });
      }
    })

    this.filteredOptions = this.mealForm.get('user').valueChanges
                                        .filter((x)=>x.length)
                                        .do((x)=>console.log(x))
                                        .switchMap(q =>this.userService.fetch({q}))

  }

  ngOnDestroy() {
    this.obs.unsubscribe()
  }


  displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }


  save() {
    this.markAsTouched(this.mealForm)
    console.log(this.mealForm.value.user)
    if(this.mealForm.valid){
      if(this.data.type == 'add') {
        this.mealService.addMeal(this.mealForm.value).subscribe(()=>this.dialogRef.close())
      } else {
        this.mealService.editMeal(this.data.meal.id, this.mealForm.value).subscribe(()=>this.dialogRef.close())
      }
    }
  }

  markAsTouched(group: FormGroup) {
    Object.keys(group.controls).map((field) => {
      const control = group.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.markAsTouched(control);
      }
    });
  }


  onNoClick(): void {
	  this.dialogRef.close()
  }

}
