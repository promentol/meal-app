const Koa = require('koa');
const app = new Koa();
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URL);
console.log(process.env.MONGO_URL)
const bodyParser = require('koa-bodyparser');
app.use(bodyParser());

const auth = require('./middleware/auth')
const AuthController = require('./routes/auth')
const UserController = require('./routes/user')
const PasswordController = require('./routes/password-recovery')
const MealController = require('./routes/meals')

const cors = require('@koa/cors');

app.use(cors())

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    console.log(e)
    ctx.status = e.status || 400
    ctx.body = {
      message: e.message
    }
  }
})

app.use(AuthController)
app.use(UserController)
app.use(PasswordController)
app.use(auth())
app.use(MealController)


// x-response-time

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// logger

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

// response

app.use(async ctx => {
  ctx.body = 'Not Found';
});

app.listen(3000);