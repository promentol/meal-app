import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Meal } from '../meal.model'
import { MealDialogComponent } from '../meal-dialog/meal-dialog.component';
import { MealService } from '../meal.service'
import * as swal from 'sweetalert'

import { 
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSnackBar
} from '@angular/material';

@Component({
  selector: 'meal-actions',
  templateUrl: './meal-actions.component.html',
  styleUrls: ['./meal-actions.component.css']
})
export class MealActionsComponent implements OnInit {

  @Input()
  meal: Meal;

  @Output() dataUpdate: EventEmitter<any> = new EventEmitter();

  constructor(
  	private dialog: MatDialog,
  	private mealService: MealService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  remove(elem) {
    swal("Are you sure?", 'Your will not be able to recover this meal!',{
      dangerMode: true,
      buttons: true,
    }).then((x)=>{
      if(x){
        this.mealService.deleteMeal(elem.id).subscribe(()=>{
          this.dataUpdate.emit()
          this.snackBar.open('Meal is deleted', '', {
            duration: 3000
          })
        })
      }
    })
  }

  edit(meal) {
    let dialogRef = this.dialog.open(MealDialogComponent, {
      //: '400px',
      data: { meal }
    });

    return dialogRef.afterClosed().subscribe(()=>{
    	this.dataUpdate.emit()
    })
  }

}
