import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../core/user.service'
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.css']
})
export class RecoverComponent implements OnInit {
  
  recoverForm: FormGroup;
  constructor(
    public fb: FormBuilder,
    public userService: UserService,
    private toasterService: ToasterService,
    private router: Router
  ) {
  	this.recoverForm = this.fb.group({
      email: ['', [Validators.required,  Validators.email]]
    });
  }

  onSubmit() {
  	if(this.recoverForm.valid) {
  		this.userService.requestPassword(this.recoverForm.value).subscribe((e) => {
          this.toasterService.pop('success', 'Congratulations', '`Instructions have been sent to your email')
          this.router.navigate(['login']);
  		})
  	}
  }

  ngOnInit() {
  }
}
